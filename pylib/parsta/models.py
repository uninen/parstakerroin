# -*- coding: utf-8 -*-
from django.db import models

from datetime import datetime

from facebookconnect.models import FacebookProfile

from common.utils import get_age

class Country(models.Model):
    name = models.CharField(max_length=80, unique=True)

    created = models.DateTimeField(default=datetime.now)
    modified = models.DateTimeField(blank=True, null=True)
    
    def __unicode__(self):
        return self.name


class City(models.Model):
    name = models.CharField(max_length=80, unique=True)

    created = models.DateTimeField(default=datetime.now)
    modified = models.DateTimeField(blank=True, null=True)
    
    def __unicode__(self):
        return self.name


GENDER_CHOICES = (
    ('M', 'Mies'),
    ('F', 'Nainen'),
)

INTERESTED_CHOICES = (
    ('M', 'Miehistä'),
    ('F', 'Naisista'),
    ('B', 'Molemmista'),
)

class Person(models.Model):
    profile = models.ForeignKey(FacebookProfile)
    first_name = models.CharField(max_length=80)
    last_name = models.CharField(max_length=80)
    dob = models.DateField(db_index=True)
    country = models.ForeignKey(Country)
    city = models.ForeignKey(City)
    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, db_index=True)
    interested_in = models.CharField(max_length=1, choices=GENDER_CHOICES, db_index=True)
    
    # optional
    short_desc = models.CharField(blank=True, max_length=140)
    long_desc = models.TextField(blank=True)
    weight = models.PositiveSmallIntegerField(blank=True, null=True)
    height = models.PositiveSmallIntegerField(blank=True, null=True)    
    
    # preferences
    age_plus = models.SmallIntegerField(default=5)
    age_minus = models.SmallIntegerField(default=5)
    limit_search = models.BooleanField(default=True)

    # stuff for management
    is_active = models.BooleanField(default=True)
    is_flagged = models.BooleanField(default=False)
    update_profile = models.BooleanField(default=False)

    created = models.DateTimeField(default=datetime.now)
    modified = models.DateTimeField(blank=True, null=True)
    

    def __unicode__(self):
        return u"%s, %iv, %s" % (self.first_name, self.age(), self.city)

    def age(self):
        return get_age(self.dob)


class Vote(models.Model):
    by = models.ForeignKey(Person, db_index=True, related_name="votes_by_me")
    to = models.ForeignKey(Person, db_index=True, related_name="votes_on_me")
    interested = models.BooleanField(default=True)
    is_match = models.BooleanField(default=False)

    created = models.DateTimeField(default=datetime.now)
    modified = models.DateTimeField(blank=True, null=True)

    class Meta:
        unique_together = ("by", "to")
    
    def __unicode__(self):
        return u"%s voted %s on %s" % (self.by, self.interested, self.to)
