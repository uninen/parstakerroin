# -*- coding: utf-8 -*-
from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.template import RequestContext
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect
from django.contrib.auth import authenticate, login, logout, REDIRECT_FIELD_NAME
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth.models import User
from django.conf import settings
from django import forms

import logging
log = logging.getLogger('facebookconnect.views')

from facebookconnect.models import FacebookProfile
from facebookconnect.views import facebook_login
from facebookconnect.forms import FacebookUserCreationForm

from common.helpers import response
from parsta.models import GENDER_CHOICES, INTERESTED_CHOICES

def homepage(request):
    return response("index.html", locals())

def profile(request):
    page = 'profile'
    user = ''

    if request.user.is_authenticated():
        user = request.user.facebook_profile
        friendList = request.user.facebook_profile.get_friends_profiles()
    else:
        print "REDIRECTING"
        return HttpResponseRedirect("/")


    return render_to_response(
        "profile.html",
        {
            'page': page,
            'USER_LOGGED_IN': request.user.is_authenticated(),
            'user': user,
            'friendList': friendList,
        },
        context_instance=RequestContext(request)
    )


class ProfileForm(forms.Form):
    gender = forms.ChoiceField(choices=GENDER_CHOICES)
    interested_in = forms.ChoiceField(choices=INTERESTED_CHOICES)
    country = forms.CharField(min_length=2, max_length=80)
    city = forms.CharField(min_length=2, max_length=80)	


def setup(request,redirect_url='/facebook/setup/parsta/',
          template_name='facebook/setup_fb.html',
          extra_context=None):
    """
    Handles a new user.
    """
    log.debug('in setup view')
    #you need to be logged into facebook.
    if not request.facebook.uid:
        log.debug('Need to be logged into facebook')
        url = reverse(facebook_login)
        if request.REQUEST.get(REDIRECT_FIELD_NAME,False):
            url += "?%s=%s" % (REDIRECT_FIELD_NAME, request.REQUEST[REDIRECT_FIELD_NAME])
        return HttpResponseRedirect(url)

    #figure out where to go after setup
    if request.REQUEST.get(REDIRECT_FIELD_NAME,False):
        redirect_url = request.REQUEST[REDIRECT_FIELD_NAME]
    elif redirect_url is None:
        redirect_url = getattr(settings, "LOGIN_REDIRECT_URL", "/")

    #check that this fb user is not already in the system
    try:
        FacebookProfile.objects.get(facebook_id=request.facebook.uid)
        # already setup, move along please
        return HttpResponseRedirect(redirect_url)
    except FacebookProfile.DoesNotExist, e:
        # not in the db, ok to continue
        pass

    #user submitted a form - which one?
    if request.method == "POST":
        log.debug('Submitted form')
        #lets setup a facebook only account. The user will have to use
        #facebook to login.
        log.debug('Facebook Only')
        profile = FacebookProfile(facebook_id=request.facebook.uid)
        user = User(username=request.facebook.uid, email=profile.email)
        user.set_unusable_password()
        user.save()
        profile.user = user
        profile.save()
        log.info("Added user and profile for %s!" % request.facebook.uid)
        user = authenticate(request=request)
        login(request, user)
        return HttpResponseRedirect(redirect_url)
        
    #user didn't submit a form, but is logged in already. We'll just link up their facebook
    #account automatically.
    elif request.user.is_authenticated():
        log.debug('Already logged in')
        try:
            request.user.facebook_profile
        except FacebookProfile.DoesNotExist:
            profile = FacebookProfile(facebook_id=request.facebook.uid)
            profile.user = request.user
            profile.save()
            log.info("Attached facebook profile %s to user %s!" % (profile.facebook_id,profile.user))

        return HttpResponseRedirect(redirect_url)
    
    # user just showed up
    else:
        log.debug('Setting up form...')
        request.user.facebook_profile = profile = FacebookProfile(facebook_id=request.facebook.uid)
        login_form = AuthenticationForm(request)
        log.debug('creating a dummy user')
        fname = lname = ''
        if profile.first_name != "(Private)":
            fname = profile.first_name
        if profile.last_name != "(Private)":
            lname = profile.last_name
        user = User(first_name=fname, last_name=lname)
    
    log.debug('going all the way...')
    
    # add the extra_context to this one
    if extra_context is None:
        extra_context = {}
    context = RequestContext(request)
    for key, value in extra_context.items():
        context[key] = callable(value) and value() or value

    template_dict = {}
    
    # we only need to set next if its been passed in the querystring or post vars
    if request.REQUEST.get(REDIRECT_FIELD_NAME, False):
        template_dict.update( {REDIRECT_FIELD_NAME: request.REQUEST[REDIRECT_FIELD_NAME]})
        
    return render_to_response(
        template_name,
        template_dict,
        context_instance=context
    )
    

def setup_parsta(request):
    form = ProfileForm()

    if request.method == "POST":
        form = ProfileForm(request.POST)
        if form.is_valid():
            return redirect('/')
            
    return response('facebook/setup_parsta.html', locals())
