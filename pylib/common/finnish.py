# -*- coding: utf-8 -*-
"""
Helpers for Finnish stuff.
"""
MONTHS = {
    1: 'tammikuu',
    2: 'helmikuu',
    3: 'maaliskuu',
    4: 'huhtikuu',
    5: 'toukokuu',
    6: 'kesakuu',
    7: 'heinakuu',
    8: 'elokuu',
    9: 'syyskuu',
    10: 'lokakuu',
    11: 'marraskuu',
    12: 'joulukuu',
}

MONTH_NAMES = {
    'tammikuu': 1,
    'helmikuu': 2,
    'maaliskuu':3 ,
    'huhtikuu': 4,
    'toukokuu': 5,
    'kesakuu':  6,
    'heinakuu': 7,
    'elokuu': 8,
    'syyskuu': 9,
    'lokakuu': 10,
    'marraskuu':11,
    'joulukuu': 12,
}

MONTH_SLUG_TO_NUMBER = {
    'tammi': 1,
    'helmi': 2,
    'maalis': 3,
    'huhti': 4,
    'touko': 5,
    'kesa': 6,
    'heina': 7,
    'elo': 8,
    'syys': 9,
    'loka': 10,
    'marras': 11,
    'joulu': 12
}

def get_finnish_month_name(month_number):
    return MONTHS[month_number]

def get_short_finnish_month_name(month_number):
    return MONTHS[month_number][:-3]

def get_finnish_month_number(month_name):
    return MONTH_NAMES[month_name]

def get_month_number_from_slug(month_slug):
    return MONTH_SLUG_TO_NUMBER[month_slug]

def shorten(text):
    """
    Shortens text in a way that it makes sense. Useful for making long titles
    shorter, for example.
    """
    if len(text) > 50:
        if '?' in text:
            parts = text.split('?')
            text = parts[0] + '?'

        if '!' in text:
            parts = text.split('!')
            text = parts[0] + '!'

        if ':' in text:
            parts = text.split(':')
            text = parts[0]

        if '.' in text:
            parts = text.split('.')
            text = parts[0]
        
    return text

def is_daylight_saving_time(date=None):
    if not date:
        date = date.today()
