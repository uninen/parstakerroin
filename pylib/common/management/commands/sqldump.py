# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.conf import settings

import os
import subprocess

class Command(BaseCommand):
    help = "Dumps project database to settings.DATA_ROOT directory"

    def handle(self, *args, **options):
        
        try:
            if settings.DATABASE_HOST:
                p = subprocess.Popen('mysqldump -h %s -u %s -p%s %s > %s%s.sql' % (settings.DATABASE_HOST, settings.DATABASE_USER, settings.DATABASE_PASSWORD, settings.DATABASE_NAME, settings.DATA_ROOT, settings.DATABASE_NAME), shell=True)
                os.waitpid(p.pid, 0)
            else:
                p = subprocess.Popen('mysqldump -u %s -p%s %s > %s%s.sql' % (settings.DATABASE_USER, settings.DATABASE_PASSWORD, settings.DATABASE_NAME, settings.DATA_ROOT, settings.DATABASE_NAME), shell=True)
                os.waitpid(p.pid, 0)
        except AttributeError:
            print 
            print "Invalid settings: You're missing DATA_ROOT setting in your settings.py"
            print 
            return

        print "Database dumped to %s%s.sql" % (settings.DATA_ROOT, settings.DATABASE_NAME)
        print "Have a nice day :)"
