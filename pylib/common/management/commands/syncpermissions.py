# -*- coding: utf-8 -*-
# based on http://www.djangosnippets.org/snippets/698/
from django.core.management.base import BaseCommand
from django.core.management import call_command
from django.contrib.contenttypes.management import update_all_contenttypes
from django.contrib.auth.management import create_permissions
from django.db.models import get_apps

from django.conf import settings

class Command(BaseCommand):
    help = "Updates permissions and contenttypes"

    def handle(self, *args, **options):
        # Add missing contenttypes
        update_all_contenttypes()

        # Add any missing permissions
        for app in get_apps():
           create_permissions(app, None, 2)
