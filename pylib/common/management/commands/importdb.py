# -*- coding: utf-8 -*-
from django.core.management.base import BaseCommand
from django.db import connection
from django.conf import settings

import os
import subprocess
from time import sleep

class Command(BaseCommand):
    help = "Drops and re-creates database from sql fixture"

    def handle(self, *args, **options):
        c = connection.cursor()
        c.execute("DROP DATABASE %s" % settings.DATABASE_NAME)
        c.execute("CREATE DATABASE `%s` DEFAULT CHARACTER SET utf8 COLLATE utf8_bin" % settings.DATABASE_NAME)
        print 'Re-created database: %s. Loading data to it... ' % settings.DATABASE_NAME,

        if settings.DATABASE_HOST:
            p = subprocess.Popen("mysql -h %s -u %s -p%s --default_character_set utf8 %s < %s%s.sql" % (settings.DATABASE_HOST, settings.DATABASE_USER, settings.DATABASE_PASSWORD, settings.DATABASE_NAME, settings.DATA_ROOT, settings.DATABASE_NAME), shell=True)
            os.waitpid(p.pid, 0)
        else:
            p = subprocess.Popen("mysql -u %s -p%s --default_character_set utf8 %s < %s%s.sql" % (settings.DATABASE_USER, settings.DATABASE_PASSWORD, settings.DATABASE_NAME, settings.DATA_ROOT, settings.DATABASE_NAME), shell=True)
            os.waitpid(p.pid, 0)

        # Apparently os operations do not hang the processing. have to do it
        # manually here. A better solution would be welcome.
        print "Done."
        print "Have a nice day :)"
        c.close()
