# -*- coding: utf-8 -*-
from django.test import Client
from django.core.handlers.wsgi import WSGIRequest
from django.db import connection
from django.conf import settings

import os
import subprocess

class RequestFactory(Client):
    """
    Class that lets you create mock Request objects for use in testing.
    
    Usage:
    
    rf = RequestFactory()
    get_request = rf.get('/hello/')
    post_request = rf.post('/submit/', {'foo': 'bar'})
    
    This class re-uses the django.test.client.Client interface, docs here:
    http://www.djangoproject.com/documentation/testing/#the-test-client
    
    Once you have a request object you can pass it to any view function, 
    just as if that view had been hooked up using a URLconf.
    
    By: Simon Willison
    From: http://www.djangosnippets.org/snippets/963/    
    """
    def request(self, **request):
        """
        Similar to parent class, but returns the request object as soon as it
        has created it.
        """
        environ = {
            'HTTP_COOKIE': self.cookies,
            'PATH_INFO': '/',
            'QUERY_STRING': '',
            'REQUEST_METHOD': 'GET',
            'SCRIPT_NAME': '',
            'SERVER_NAME': 'testserver',
            'SERVER_PORT': 80,
            'SERVER_PROTOCOL': 'HTTP/1.1',
        }
        environ.update(self.defaults)
        environ.update(request)
        return WSGIRequest(environ)

def use_production_db(verbose=False):
    """
    A helper method for using production data in tests. Changes the whole test
    database to given production database in settings.FIXTURE_DIRS[0].
    """
    cursor = connection.cursor()
    
    # instead fo dropping the database and re-creating it, just iterating the 
    # tables as the former does not seem to work with test environment
    cursor.execute("use %s" % settings.DATABASE_NAME)
    cursor.execute("SHOW TABLES")
    rows = cursor.fetchall()
    for row in rows:
        if verbose:
            print "Dopping table %s" % row[0]
        cursor.execute("DROP TABLE `%s`" % row[0])
    cursor.execute("use %s" % settings.DATABASE_NAME)
    print ""
    print "Inserting production SQL to %s... " % settings.DATABASE_NAME, 
    
    if verbose:
        print ""
        print ""
        print "Executing:"
        print "mysql -h %s -u %s -p%s --default_character_set utf8 %s < %s/%s.sql" % (settings.DATABASE_HOST, settings.DATABASE_USER, settings.DATABASE_PASSWORD, settings.DATABASE_NAME, settings.FIXTURE_DIRS[0], datatase_name)
        print ""
    
    datatase_name = settings.DATABASE_NAME.split('test_')[1]
    
    if settings.DATABASE_HOST:
        p = subprocess.Popen("mysql -h %s -u %s -p%s --default_character_set utf8 %s < %s/%s.sql" % (settings.DATABASE_HOST, settings.DATABASE_USER, settings.DATABASE_PASSWORD, settings.DATABASE_NAME, settings.FIXTURE_DIRS[0], datatase_name), shell=True)
        os.waitpid(p.pid, 0)
    else:
        p = subprocess.Popen("mysql -u %s -p%s --default_character_set utf8 %s < %s/%s.sql" % (settings.DATABASE_USER, settings.DATABASE_PASSWORD, settings.DATABASE_NAME, settings.FIXTURE_DIRS[0], datatase_name), shell=True)
        os.waitpid(p.pid, 0)

    print "Done."
    cursor = connection.cursor()        
    cursor.execute("use %s" % settings.DATABASE_NAME)
    cursor.execute("SHOW TABLES")
    rows = cursor.fetchall()
    if verbose:
        print "%i rows in test db." % len(rows)
