# -*- coding: utf-8 -*-
from django.conf import settings

def show_toolbar(request):
    """
    Custom django-debug-toolbar method.
    """
    if not settings.DEBUG:
        return False
    if request.is_ajax() and not \
        request.path.startswith(os.path.join('/', debug_toolbar.urls._PREFIX)):
        # Allow ajax requests from the debug toolbar
        return False
#    if request.path.startswith('/localmedia/'):
#        return False
    if request.path in ('/hallinto/', '/kirjaudu/'):
        #'/favicon.ico', 
        return False
    if not request.META.get('REMOTE_ADDR') in settings.INTERNAL_IPS:
        #print "No toolbar for %s" % request.path
        return False

    print "Yest for toolbar for path %s" % request.path
    return True
