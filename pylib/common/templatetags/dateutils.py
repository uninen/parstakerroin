from django import template

register = template.Library()

from django.utils.safestring import mark_safe
from django.template.defaultfilters import stringfilter

from datetime import date
from dateutil.relativedelta import relativedelta

@register.filter(name='todate')
@stringfilter
def todate(value):
    return date(*[int(x) for x in value.split('-')])

@register.filter(name='date_to_age')
def date_to_age(value):
    r = relativedelta(date.today(), value)
    return r.years
