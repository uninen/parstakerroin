# -*- coding: utf-8 -*-
from django.template import Library
import re
from datetime import datetime, timedelta
from django.template.defaultfilters import timesince
from django.utils.translation import ugettext

register = Library()

@register.filter
def deslugify(string):
    """Converts a URL slug into an imitation of a human sentence. Ideally this should reverse the output of the slugify filter."""
    return re.sub('[_-]', ' ', string)
    
@register.filter
def approx_timesince(d, now=None):
    """
    A more concise version of the timesince filter which does not display 'fractions' (e.g. "1 week, 3 days" becomes just "1 week").
    This works by crudely chopping off any content after the comma in the result returned by timesince.
    """
    
    delimiter = ugettext(', %(number)d %(type)s')[0]
    return unicode(timesince(d, now).split(delimiter)[0])

@register.filter
def approx_timeuntil(d, now=None):
    """
    Like approx_timesince, but returns a string measuring the time until the given time.
    """
    delimiter = ugettext(', %(number)d %(type)s')[0]
    return unicode(timeuntil(d, now).split(delimiter)[0])