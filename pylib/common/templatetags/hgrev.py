# -*- coding: utf-8 -*-
from django.template import Library

from mercurial import ui, hg
from mercurial.hgweb import common
from datetime import datetime

REPO_DIR = '/home/syneusfi/new_intra'

register = Library()

@register.simple_tag
def hg_revision():
    u = ui.ui()
    repository = hg.repository(u, REPO_DIR)
    revision_number = repository.changectx('tip').rev()
    return revision_number

@register.inclusion_tag('common/templatetags/hg_date.html')
def hg_date():
    mt = common.get_mtime(REPO_DIR)
    last_updated = datetime.fromtimestamp(mt)
    return {'last_updated': last_updated}
