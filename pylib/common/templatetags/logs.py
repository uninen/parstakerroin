# -*- coding: utf-8 -*-
from django.template import Library

from common.models import LogEntry

register = Library()

@register.inclusion_tag('common/templatetags/logs.html', takes_context=True)
def logs_for_page(context):
    try:
        request = context['request']
        entries = LogEntry.objects.filter(path=request.path)[:25]
        return {'entries': entries}
    except KeyError:
        return {'entries': None}
