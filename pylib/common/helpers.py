# -*- coding: utf-8 -*-
from django.shortcuts import render_to_response
from django.template import Context, RequestContext, loader
from django.http import HttpResponse, HttpResponsePermanentRedirect, \
HttpResponseGone
from django.core.urlresolvers import reverse, NoReverseMatch
from django.utils.html import urlize

from markdown import markdown
from common.lib.smartypants import smartyPants

def response(template, args):
    """
    Wraps render_to_response with RequestContext.
    """
    return render_to_response(template, args, 
        context_instance=RequestContext(args['request']))

def to_string(template, args):
    """
    Renders given template and arguments to a string.
    """
    t = loader.get_template(template)
    c = Context(args)
    return t.render(c)

def js_response(data):
    """
    Returns given data with JS-mimetype. Use to_string() if you want to 
    use template with this.
    """
    response = HttpResponse(mimetype='text/javascript')
    response.write(data)    
    return response

def auto_redirect(request, to, show_message=False, message=None, **kwargs):
    """
    Redirects given URL.
    First tries to reverse-map the given URL. IF no match is found, 
    then redirects via permanent redirect .
    
    If given URL is None, then returns status code 410 (Gone).
    """
    # If given name is none, we'll give 410 (Gone)
    if to is None:
        return HttpResponseGone()

    try:
        # If not, we'll try to reverse the name
        url = reverse(to)
        if show_message:
            if message is None:
                message = "Tämän sivun osoite on muuttunut. Päivitä \
                    kirjanmerkkisi!"
            request.user.message_set.create(message=message)
        return HttpResponsePermanentRedirect(url)
    except NoReverseMatch:
        # If reverse fails, redirect wirh permanent redirect (301)
        return HttpResponsePermanentRedirect(to % kwargs)

def text_to_html(text):
    html = smartyPants(markdown(urlize(text)))
    return html
