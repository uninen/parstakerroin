# -*- coding: utf-8 -*-
from django.http import HttpResponseBadRequest
from django.utils.functional import wraps

from common.models import LogEntry


def log_view(view):
    """
    Logs any acces to given view into database.
    """
    def wrapper(request, *args, **kwargs):
        LogEntry.objects.create(
            entry_type='INFO',
            message="Visited page",
            user=request.user,
            path=request.path,
        )
        return view(request, *args, **kwargs)
    return wraps(view)(wrapper)


def ajax_required(view):
    """
    Returns HttpResponseBadRequest (400) if not request.is_ajax()

    From: http://www.djangosnippets.org/snippets/771/
    """    
    def wrap(request, *args, **kwargs):
            if not request.is_ajax():
                return HttpResponseBadRequest()
            return view(request, *args, **kwargs)
    wrap.__doc__=view.__doc__
    wrap.__name__=view.__name__
    return wrap
