# -*- coding: utf-8 -*-
from mercurial import ui, hg
from mercurial.hgweb import common
from datetime import datetime

def get_hg_revision_and_date(repo_dir):
    """
    Returns revision number as integer, update time as datetime object.
    """
    u = ui.ui()
    repository = hg.repository(u, repo_dir)
    HG_REVISION_NUMBER = repository.changectx('tip').rev()
    
    mt = common.get_mtime(repo_dir)
    HG_LAST_UPDATED = datetime.fromtimestamp(mt)
    return (HG_REVISION_NUMBER, HG_LAST_UPDATED)
