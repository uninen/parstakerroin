# -*- coding: utf-8 -*-
from django.contrib import admin

from models import Setting, LogEntry

class SettingAdmin(admin.ModelAdmin):
    pass

class LogEntryAdmin(admin.ModelAdmin):
    list_display = ('timestamp', 'user', 'entry_type', 'message', 'path', '_has_extra_message')
    list_filter = ('entry_type', 'user', 'timestamp')
    search_fields = ('message', 'extra_message', 'path')

admin.site.register(LogEntry, LogEntryAdmin)
admin.site.register(Setting, SettingAdmin)
