# -*- encoding: utf-8 -*-
from urllib import quote
from django.conf import settings
from django.contrib.auth import REDIRECT_FIELD_NAME
from django.http import HttpResponseRedirect

from common.models import LogEntry

def allow_anonymous(view_func):
    view_func.allow_anonymous = True
    return view_func

class RequireLogin(object):
    """
    A middlewae to lock site up by default. Decorate any anonymous views with 
    @allow_anonymous

    Based on code by Nathan Ostgard
    http://nathanostgard.com/archives/2007/7/22/private-by-default/
    """
    def process_view(self, request, view_func, view_args, view_kwargs):
        if not request.path.startswith(settings.LOGIN_URL) and \
            not request.user.is_authenticated() and \
            not request.path.startswith(settings.MEDIA_URL) and \
            not getattr(view_func, 'allow_anonymous', False):
            url = '%s?%s=%s' % (settings.LOGIN_URL, REDIRECT_FIELD_NAME, \
                quote(request.get_full_path()))
            LogEntry.objects.create(
                entry_type='AUTH',
                message = 'RequireLogin redirect from %s' % request.path,
            )
            return HttpResponseRedirect(url)
