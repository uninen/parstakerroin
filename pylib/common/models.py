# -*- encoding: utf-8 -*-
from django.contrib.auth.models import User
from django.conf import settings
from django.db import models

from datetime import datetime

ENTRY_TYPES = (
    ('404', '404'),
    ('ERROR', 'Virhe'),
    ('WARNING', 'Varoitus'),
    ('INFO', 'Tapahtuma'),
    ('USER', 'Käyttäjä'),
    ('DEBUG', 'Debug'),
    ('AUTH', 'Autentikointi'),
    ('CAREER', 'Rekry'),
)

class LogEntry(models.Model):
    """
    Model for logging variois events on the site.
    """
    timestamp = models.DateTimeField('Aikaleima', default=datetime.now)
    entry_type = models.CharField('Tyyppi', choices=ENTRY_TYPES, max_length=10, db_index=True)
    message = models.CharField('Viesti', max_length=78)
    extra_message = models.TextField(blank=True)
    user = models.ForeignKey(User, blank=True, null=True, related_name="logentries")
    path = models.CharField(blank=True, max_length=200, db_index=True)

    class Meta:
        ordering = ('-timestamp',)

    def __unicode__(self):
        return "%s %s %s" % (self.timestamp, self.entry_type, self.message)

    def _has_extra_message(self):
        if self.extra_message:
            return '<img src="%simg/admin/icon-yes.gif" alt="Yes" />' % settings.ADMIN_MEDIA_PREFIX
        else:
            return ''
    _has_extra_message.allow_tags = True
    _has_extra_message.short_description = 'Lisäviesti'

class Setting(models.Model):
    """
    Model for generic site-wide settings.
    """
    name = models.CharField(max_length=30, unique=True, db_index=True)
    value = models.TextField()
    
#    class Admin:
#        pass

    def __unicode__(self):
        return self.name