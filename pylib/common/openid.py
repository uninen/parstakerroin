# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.http import HttpResponseForbidden
from django.http import HttpResponseRedirect as Redirect
from django.core.cache import cache
from django.conf import settings
from django.contrib.auth import authenticate

from datetime import datetime, timedelta
import functools, sha

from django_openid.auth import AuthConsumer
from common.models import LogEntry
from common.helpers import to_string, response

class IntraConsumer(AuthConsumer):

    redirect_after_logout = '/kirjaudu/ulos/ok/'
    key_field = 'username'
    warning_requests = 3
    requests = 10
    minutes = 2
    
    prefix = 'rl-' # Prefix for memcache key
    expire_after = (minutes + 1) * 60

    counts = 0
    
    def real_do_login(self, request, extra_message=None):
        if request.method == 'POST' and \
                request.POST.get('username', '').strip():
            # Do a username/password login instead
            user = authenticate(
                username = request.POST.get('username'),
                password = request.POST.get('password')
            )
            if not user:
                return self.show_login(request, self.bad_password_message)
            else:
                self.log_in_user(request, user)
                return self.on_login_complete(request, user, openid=None)
        else:
            return super(AuthConsumer, self).do_login(request, extra_message)

    def do_login(self, request, extra_message=None):
        if not self.should_ratelimit(request):
            return self.real_do_login(request, extra_message)
        
        counts = [int(c) for c in self.get_counters(request).values()]
        self.counts = sum(counts)
        
        # Increment rate limiting counter
        self.cache_incr(self.current_key(request))
        
        # Have they failed?
        if self.counts >= self.requests:
            return self.disallowed(request)
        
        if self.counts >= self.warning_requests:
            request.throttle_warning = True
            LogEntry.objects.create(
                entry_type='AUTH',
                message = '%s sai varoituksen kirjautumislomakkeella' % request.POST.get('username', '-NA-'),
                path = '/kirjaudu/',
                extra_message="IP: %s\n\nUser agent: %s" % (request.META.get('REMOTE_ADDR', ''), request.META.get('HTTP_USER_AGENT', '<unknown>'))
            )
        return self.real_do_login(request, extra_message)

    def on_login_complete(self, request, user, openid=None):

        if openid:
            openid_str = u" OpenID:llä %s" % openid
        else:
            openid_str = u""

        LogEntry.objects.create(
            entry_type='AUTH',
            message = u'%s kirjautui sisään%s' % (user, openid_str),
            user = user,
        )

        response = self.redirect_if_valid_next(request)
        if not response:
            response = Redirect(self.after_login_redirect_url)
        return response

    def do_ulos(self, request):
        user = request.user
        LogEntry.objects.create(
            entry_type='AUTH',
            message = '%s kirjautui ulos' % user,
            user = user,
        )
        return self.do_logout(request)

    def show_unknown_openid(self, request, openid):
        return response('django_openid/unknown_openid.html', locals())

    ### Ratelimit methods
    def cache_get_many(self, keys):
        return cache.get_many(keys)
    
    def cache_incr(self, key):
        # memcache is only backend that can increment atomically
        try:
            # add first, to ensure the key exists
            cache._cache.add(key, '0', time=self.expire_after)
            cache._cache.incr(key)
        except AttributeError:
            cache.set(key, cache.get(key, 0) + 1, self.expire_after)
    
    def should_ratelimit(self, request):
        return request.method == 'POST'
    
    def get_counters(self, request):
        return self.cache_get_many(self.keys_to_check(request))
    
    def keys_to_check(self, request):
        extra = self.key_extra(request)
        now = datetime.now()
        return [
            '%s%s-%s' % (
                self.prefix,
                extra,
                (now - timedelta(minutes = minute)).strftime('%Y%m%d%H%M')
            ) for minute in range(self.minutes + 1)
        ]
    
    def current_key(self, request):
        return '%s%s-%s' % (
            self.prefix,
            self.key_extra(request),
            datetime.now().strftime('%Y%m%d%H%M')
        )
    
    def key_extra(self, request):
        extra = request.META.get('REMOTE_ADDR', '')
        if self.key_field:
            value = sha.new(request.POST.get(self.key_field, '')).hexdigest()
            extra += '-' + value
        return extra
    
    def disallowed(self, request):
        MEDIA_URL = settings.MEDIA_URL
        username = request.POST.get('username', None)
        if username:
            try:
                u = User.objects.get(username=username)
                u.is_active = False
                u.save()
                extra_message = "IP: %s\n\nUser agent: %s" % (request.META.get('REMOTE_ADDR', ''), request.META.get('HTTP_USER_AGENT', '<unknown>'))
                LogEntry.objects.create(
                    entry_type='AUTH',
                    message = u'Käyttäjän %s tunnus lukittu kirjauslomakkeella' % u.username,
                    path = '/kirjaudu/',
                    extra_message=extra_message,
                )
            except User.DoesNotExist:
                pass
        return HttpResponseForbidden(to_string('registration/account_locked.html', locals()))
