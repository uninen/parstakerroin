# -*- coding: utf-8 -*-
from django.conf import settings

from common.helpers import to_string

def hg(request):
    last_updated = to_string('common/templatetags/hg_date.html', {'last_updated': settings.HG_LAST_UPDATED})
    return {
        'HG_REVISION_NUMBER': settings.HG_REVISION_NUMBER,
        'HG_LAST_UPDATED': last_updated,
    }
