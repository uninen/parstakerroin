# -*- coding: utf-8 -*-
"""
Converts database from latin1 to utf8
(c) Ville Säävuori, ville@syneus.fi, 2008
"""
from django.shortcuts import get_object_or_404, render_to_response
from django.db import connection

def convert_db():
    cursor = connection.cursor()
    cursor.execute("SHOW TABLES")
    rows = cursor.fetchall()
    for row in rows:
        cursor.execute("ALTER TABLE `%s` CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin" % row[0])
        print "Converted %s" % row[0]
