"""
>>> from datetime import time
>>> from common.hello import hello

# It should be 'huomenta' at 9:23
>>> greet = hello(time(9, 23))
>>> greet.endswith('huomenta')
True
"""