# -*- coding: utf-8 -*-
from common.models import LogEntry

from datetime import datetime, time
import random

def hello(when=None):
    """
    A simple method that greets you in the correct day of 
    time (morning, afternoon, etc), depending on when you call it.

    In Finnish:

    06-10 - Aamu
    10-12 - Aamupäivä
    12-13 - Keskipäivä
    13-16 - Iltapäivä
    17-18 - Alkuilta
    18-22 - Ilta
    22-00 - Myöhäisilta
    00-01 - Keskiyö
    01-04 - Yö
    04-06 - Aamuyö
    """
    
    def between(time_now, h1, h2):
        h1_time = time(h1,0)
        h2_time = time(h2,0)
        if h2 == 0:
            h2_time = time(23,59)            
        return time_now > h1_time and time_now < h2_time
    # usage: between(hour (datetime.time), from (int), to (int))

    greets = [
        u'hyvää',
        u'hyvää',
        u'iloista',
        u'mukavaa',
    ]

    if not when:
        when = datetime.now().time()

    if between(when, 6, 10):
        greets.append('kirkasta')
        return u"%s huomenta" % random.choice(greets)
    elif between(when, 10, 12):
        greets.append('pirtsakkaa')
        return u"%s aamupäivää" % random.choice(greets)
    elif between(when, 12, 13):
        return u"%s keskipäivää" % random.choice(greets)
    elif between(when, 13, 16):
        return u"%s iltapäivää" % random.choice(greets)
    elif between(when, 16, 18):
        return u"%s alkuiltaa" % random.choice(greets)
    elif between(when, 18, 22):
        return u"%s iltaa" % random.choice(greets)
    elif between(when, 22, 0):
        greets.append(u'stressitöntä')
        return u"%s myöhäisiltaa" % random.choice(greets)
    elif between(when, 0, 1):
        return u"%s keskiyötä" % random.choice(greets)
    elif between(when, 1, 4):
        greets.append('rauhallista')
        greets.append('hiljaista')
        return u"%s yötä" % random.choice(greets)
    elif between(when, 4, 6):
        greets.append('rauhallista')
        return u"%s aamuyötä" % random.choice(greets)
    
    LogEntry.objects.create(
        entry_type='WARNING',
        message = 'Hello failed for time %s' % when,
        )    
    return u"Hyvää päivää"

if __name__ == "__main__":
	print hello()
