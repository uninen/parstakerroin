============
Kasvokerroin
============

A simple dating-oriented Facebook app. Browse trough other users FB photos and select which you are interested in. If the other person is interested in you too, you match and you get a link to her FB profile.


Basic idea and workflow
=======================

1. User logs in via FB
	a. If user doesn't have a `parsta.User` profile, -> step 2.
	b. If user already has `parsta.User` profile, jump to step 3.
2. User creates a `parsta.User` profile by selecting:
	* Gender 
	* Interested in
	* Country
	* City

	Optionally
	* Height
	* Weight
	* Short description
	* Long description

3. User is presented with the index page that has:
	* My settings -link
	* Browse people -link that is enabled by default
		* User can browse other registered user profiles and select ones that she LIKEs

TODO: Finnish this

TODO: Food + Movie plan