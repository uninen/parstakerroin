# -*- coding: utf-8 -*-
from django.conf.urls.defaults import *
from django.conf import settings
from django.views.generic.simple import direct_to_template

from django.contrib import admin
admin.autodiscover()

if settings.DEBUG:
    # Serve all local files from MEDIA_ROOT below /localmedia/
    urlpatterns = patterns('', 
        (r'^localmedia/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
    )
else:
    urlpatterns = patterns('',)

urlpatterns += patterns('',
    url(r'^$', 'parsta.views.homepage'),
#    url(r'^kasvokerroin/$', 'parsta.views.homepage'),
    (r'^accounts/profile', 'parsta.views.profile'),
    (r'^facebook/', include('facebookconnect.urls')),
    (r'^hallinta/', include(admin.site.urls)),
    (r'^xd_receiver\.htm$', direct_to_template, {'template': 'facebook/xd_receiver.htm'}),
)
