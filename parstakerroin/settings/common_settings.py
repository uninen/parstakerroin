# -*- coding: utf-8 -*-
ADMINS = (
    (u'Syneus Tuotanto', 'tuotanto@syneus.fi'),
)

MANAGERS = ADMINS

TIME_ZONE = 'Europe/Helsinki'
LANGUAGE_CODE = 'fi'
SITE_ID = 1
USE_I18N = True

ROOT_URLCONF = 'parstakerroin.urls'

TEMPLATE_DIRS = (
    '/home/parstakerroin/parstakerroin/templates/',
)

FIXTURE_DIRS = (
    '/home/parstakerroin/parstakerroin/data/fixtures/',
)

INTERNAL_IPS = (
    '127.0.0.1',
    '10.0.0.22',
    '10.0.0.10',
    '192.168.0.100',
)

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    "django.core.context_processors.auth",
    "django.core.context_processors.debug",
    "django.core.context_processors.i18n",
    "django.core.context_processors.media",
    "common.context_processors.hg",
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',

    'debug_toolbar',
    'common',
    
    'facebook',
    'facebookconnect',    
    'parsta',
)

LOGIN_REDIRECT_URL = '/'

# Non-Django settings

TEST_APPS = (
    'common',
    'parsta',
)

# Replace with keys from Facebook
FACEBOOK_API_KEY = 'c3c1a4154e342fad319bf0c449bfa9f0'
FACEBOOK_SECRET_KEY = '9d839f99ecc0a5b5450664187a5fe97d'
FACEBOOK_INTERNAL = True

NUM_FRIEND_RETRIEVE_LIMIT = 9

#Cache facebook info for x seconds
FACEBOOK_CACHE_TIMEOUT = 1800
