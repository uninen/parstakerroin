# -*- coding: utf-8 -*-
from parstakerroin.settings.common_settings import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG
LOCALDEV=True

DATABASE_ENGINE = 'sqlite3'           # 'postgresql_psycopg2', 'postgresql', 'mysql', 'sqlite3' or 'oracle'.
DATABASE_NAME = '/home/parstakerroin/parstakerroin/data/sqlite.db'             # Or path to database file if using sqlite3.
DATABASE_USER = ''             # Not used with sqlite3.
DATABASE_PASSWORD = ''         # Not used with sqlite3.
DATABASE_HOST = ''             # Set to empty string for localhost. Not used with sqlite3.
DATABASE_PORT = ''             # Set to empty string for default. Not used with sqlite3.

MEDIA_ROOT = '/home/parstakerroin/parstakerroin/media/'
MEDIA_URL = '/localmedia/'
ADMIN_MEDIA_PREFIX = '/media/'

SECRET_KEY = '#hq3^zlu^=94s^)yki$#tso@5zu=ts&d&97c_0zl-x&x+zcxvz'

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'facebook.djangofb.FacebookMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'facebookconnect.middleware.FacebookConnectMiddleware',
)

AUTHENTICATION_BACKENDS = (
    'facebookconnect.models.FacebookBackend',
    'django.contrib.auth.backends.ModelBackend',
)

from common.hg import get_hg_revision_and_date
HG_REVISION_NUMBER, HG_LAST_UPDATED = get_hg_revision_and_date('/home/parstakerroin/parstakerroin/')

DATA_ROOT = '/home/parstakerroin/parstakerroin/data/'
